/******************************************************************************/
/*
* @file   soa.hpp
* @author Aditya Harsh
* @brief  Struct to Struct of Arrays convertor.

          Copyright (c) 2019 Aditya Harsh
          Permission is hereby granted, free of charge, to any person obtaining 
          a copy of this software and associated documentation files 
          (the "Software"), to deal in the Software without restriction, 
          including without limitation the rights to use, copy, modify, merge, 
          publish, distribute, sublicense, and/or sell copies of the Software, 
          and to permit persons to whom the Software is furnished to do so, 
          subject to the following conditions:
          The above copyright notice and this permission notice shall be 
          included in all copies or substantial portions of the Software.
          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
          EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
          MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
          NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
          BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
          ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
          CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
          SOFTWARE.
*/
/******************************************************************************/

#ifndef SOA_HPP
#define SOA_HPP

#include <array>                 // std::array
#include <unordered_set>         // std::unordered_set
#include <utility>               // std::exchange
#include <cassert>               // assert
#include <limits>                // std::numeric_limits<T>::max

#define BOOST_PFR_USE_CPP17 1
#include <boost/pfr/precise.hpp> // boost::pfr::...

namespace atl
{
    namespace detail
    {
        template <typename T, std::size_t MaxElements, std::size_t... Is>
        constexpr auto struct_to_tuple_of_arrays_type_impl(std::index_sequence<Is...>) noexcept
        {
            return std::tuple<std::array<boost::pfr::tuple_element_t<Is, T>, MaxElements>...>{};
        }

        template <typename T, std::size_t MaxElements>
        constexpr auto struct_to_tuple_of_arrays_type() noexcept
        {
            constexpr auto size = boost::pfr::tuple_size_v<T>;
            return struct_to_tuple_of_arrays_type_impl<T, MaxElements>(std::make_index_sequence<size>());
        }

        template <std::size_t... Args>
        struct is_unique
        {
            static constexpr bool value = true;
        };

        template <std::size_t A, std::size_t B, std::size_t... Args>
        struct is_unique<A, B, Args...>
        {
            static constexpr bool value = (A != B) && is_unique<B, Args...>::value;
        };

        template <std::size_t A>
        constexpr auto smallest_utype() noexcept
        {
            if constexpr (A <= std::numeric_limits<uint8_t>::max())
            {
                return std::uint8_t{};
            }
            else if constexpr (A <= std::numeric_limits<uint16_t>::max())
            {
                return std::uint16_t{};
            }
            else if constexpr (A <= std::numeric_limits<uint32_t>::max())
            {
                return std::uint32_t{};
            }
            else
            {
                return std::uint64_t{};
            }
        }
    }
    
    /**
     * @brief Generic Struct of Arrays class that converts PODs into a tuple of std::arrays.
     * 
     * @tparam MaxElements The total number of elements that will be supported.
     * @tparam T Which POD to convert.
     */
    template <std::size_t MaxElements, typename T>
    class soa final
    {
        static_assert(MaxElements > 0, "SoA must have at least one element.");
        static_assert(boost::pfr::tuple_size_v<T> <= sizeof(std::size_t) * 8, "T has too many fields: max is sizeof(std::size_t) * 8");
    private:
        friend class entity;
    public:
        using underlying = T;
    private:
        using array_size_type = decltype(detail::smallest_utype<MaxElements>());
        using element_size_type = decltype(detail::smallest_utype<boost::pfr::tuple_size_v<T>>());
    private:
        /**
         * @brief Prevents crash if entity is destroyed after origin soa is destroyed.
         * 
         */
        class entity_base
        {
            friend class soa;
        protected:
            soa& m_soa;
            const array_size_type m_id;
        protected:
            entity_base(soa& origin, array_size_type id) noexcept : m_soa{origin}, m_id{id} {}
        };
    private:
        std::array<element_size_type, MaxElements> m_masks;
        using tuple_of_arrays = decltype(detail::struct_to_tuple_of_arrays_type<T, MaxElements>());
        tuple_of_arrays m_data;
        std::unordered_set<const entity_base*> m_active_entities;
    public:
        /**
         * @brief Entity class which wraps an SoA.
         * 
         * @tparam Members Which member variables this entity will use.
         */
        template <std::size_t... Members>
        class entity final : public entity_base
        {
            friend class soa;
        public:
            using underlying = T;
        private:
            /**
             * @brief No default constructor.
             * 
             */
            entity() = delete;

            /**
             * @brief No copy constructor.
             * 
             */
            entity(const entity&) = delete;

            /**
             * @brief No assignment.
             * 
             * @return entity& 
             */
            entity& operator=(const entity&) = delete;

            /**
             * @brief No move assignment.
             * 
             * @return entity& 
             */
            entity& operator=(entity&&) = delete;
            
            /**
             * @brief Constructs a new entity.
             * 
             * @param origin The SoA object this entity was created from.
             * @param id The id of the entity.
             */
            entity(soa& origin, array_size_type id) noexcept : entity_base{origin, id}
            {
                if (*this)
                {
                    entity_base::m_soa.m_active_entities.insert(this);
                }
            }
        public:
            /**
             * @brief Construct a new entity by moving.
             * 
             * @param other The entity to construct from.
             */
            entity(entity&& other) noexcept : entity_base{other.m_soa, std::exchange(const_cast<array_size_type&>(other.m_id), MaxElements)}
            {
                if (*this)
                {
                    entity_base::m_soa.m_active_entities.insert(this);
                    entity_base::m_soa.m_active_entities.erase(&other);
                }
            }

            /**
             * @brief Removes the entity from the SoA.
             * 
             */
            ~entity() noexcept
            {
                if (entity_base::m_id < MaxElements)
                {
                    entity_base::m_soa.m_masks[entity_base::m_id] = 0;
                    entity_base::m_soa.m_active_entities.erase(this);
                }
            }

            /**
             * @brief Returns whether or not this entity is valid.
             * 
             * @return true 
             * @return false 
             */
            [[nodiscard]] constexpr operator bool() const noexcept
            {
                return entity_base::m_id < MaxElements;
            }

            /**
             * @brief Get a reference to a member.
             * 
             * @tparam index Which member to get.
             * @return const boost::pfr::tuple_element_t<index, T>& Reference to the member.
             */
            template <std::size_t index>
            [[nodiscard]] constexpr auto get_member() -> boost::pfr::tuple_element_t<index, T>&
            {
                assert(*this);
                assert(entity_base::m_soa.m_masks[entity_base::m_id] & (static_cast<element_size_type>(1) << static_cast<element_size_type>(index)));
                return std::get<index>(entity_base::m_soa.m_data)[entity_base::m_id];
            }

            /**
             * @brief Get a const reference to a member.
             * 
             * @tparam index Which member to get.
             * @return const boost::pfr::tuple_element_t<index, T>& Reference to the member.
             */
            template <std::size_t index>
            [[nodiscard]] constexpr auto get_member() const -> const boost::pfr::tuple_element_t<index, T>&
            {
                assert(*this);
                assert(entity_base::m_soa.m_masks[entity_base::m_id] & (static_cast<element_size_type>(1) << static_cast<element_size_type>(index)));
                return std::get<index>(entity_base::m_soa.m_data)[entity_base::m_id];
            }

            /**
             * @brief Performs an operation on all members of the entity.
             * 
             * @tparam Op The operation type.
             * @param op The operation.
             */
            template <typename Op>
            constexpr auto for_each_member(Op&& op) -> void
            {
                assert(*this);
                (op(std::get<Members>(entity_base::m_soa.m_data)[entity_base::m_id]), ...);
            }

            /**
             * @brief Performs an operation on all members of the entity.
             * 
             * @tparam Op The operation type.
             * @param op The operation.
             */
            template <typename Op>
            constexpr auto for_each_member(Op&& op) const -> void
            {
                assert(*this);
                (op(std::get<Members>(entity_base::m_soa.m_data)[entity_base::m_id]), ...);
            }

            /**
             * @brief Emplaces all arguments into the corresponding members of the element.
             * 
             * @tparam Args The argument types.
             * @param args The arguments.
             */
            template <typename... Args>
            constexpr auto emplace(Args&&... args) noexcept -> void
            {
                assert(*this);
                ((std::get<Members>(entity_base::m_soa.m_data)[entity_base::m_id] = std::move(args)), ...);
            }

            /**
             * @brief Sets all arguments to the corresponding members of the element.
             * 
             * @tparam Args The argument types.
             * @param args The arguments.
             */
            template <typename... Args>
            constexpr auto assign(const Args&... args) -> void
            {
                assert(*this);
                ((std::get<Members>(entity_base::m_soa.m_data)[entity_base::m_id] = args), ...);
            }

            /**
             * @brief Gets all components of the object as a tuple.
             * 
             * @return std::tuple<boost::pfr::tuple_element_t<Members, T>...> All the components.
             */
            [[nodiscard]] constexpr auto get_all() const -> std::tuple<boost::pfr::tuple_element_t<Members, T>...>
            {
                return std::make_tuple((std::get<Members>(entity_base::m_soa.m_data)[entity_base::m_id])...);
            }
        };
    private:
        /**
         * @brief No copy constructor.
         * 
         */
        soa(const soa&) = delete;

        /**
         * @brief No move constructor.
         * 
         */
        soa(soa&&) = delete;

        /**
         * @brief No assignment.
         * 
         * @return soa& 
         */
        soa& operator=(const soa&) = delete;

        /**
         * @brief No move assignment.
         * 
         * @return soa& 
         */
        soa& operator=(soa&&) = delete;
    public:
        /**
         * @brief Initializes the Struct of Arrays object.
         * 
         */
        soa() : m_masks{}, m_data{}
        {
            m_active_entities.reserve(MaxElements);
        }
        
        /**
         * @brief Resets all active entities.
         * 
         */
        ~soa()
        {
            for (auto i : m_active_entities)
            {
                const_cast<array_size_type&>(i->m_id) = MaxElements;
            }
        }

        /**
         * @brief Initializes a new entity object with the requested members.
         * 
         * @tparam Members The members of the entity.
         * @return entity<Members...> The new entity;
         */
        template <std::size_t... Members>
        [[nodiscard]] constexpr auto create_entity() noexcept -> entity<Members...>
        {
            static_assert(sizeof...(Members) <= boost::pfr::tuple_size_v<T>, "Attempting to add too many members.");
            static_assert(detail::is_unique<Members...>::value, "Attempting to add duplicate members.");

            auto slot = find_open_slot();
            assert(slot < MaxElements);
            create_entity_impl<Members...>(slot);
            return entity<Members...>{*this, slot};
        }

        /**
         * @brief Performs an operation across a given member of all entities.
         * 
         * @tparam index The index of one of the constructing parameter pack types.
         * @tparam Op The operation type.
         * @param op The operation to be performed.
         * @param process_all If true, all members are processed regardless of whether or not they are active.
         */
        template <std::size_t index, typename Op>
        constexpr auto for_each(Op&& op, bool process_all = false) -> void
        {
            auto& arr = std::get<index>(m_data);

            if (process_all)
            {
                for (auto& i : arr)
                {
                    op(i);
                }
            }
            else
            {
                auto mask = static_cast<element_size_type>(1) << static_cast<element_size_type>(index);

                for (array_size_type i = 0; i < static_cast<array_size_type>(MaxElements); ++i)
                {
                    if (m_masks[i] & mask)
                    {
                        op(arr[i]);
                    }
                }
            }
        }

        /**
         * @brief Performs an operation across a given member of all entities.
         * 
         * @tparam index The index of one of the constructing parameter pack types.
         * @tparam Op The operation type.
         * @param op The operation to be performed.
         * @param process_all If true, all members are processed regardless of whether or not they are active.
         */
        template <std::size_t index, typename Op>
        constexpr auto for_each(Op&& op, bool process_all = false) const -> void
        {
            const auto& arr = std::get<index>(m_data);

            if (process_all)
            {
                for (const auto& i : arr)
                {
                    op(i);
                }
            }
            else
            {
                auto mask = static_cast<element_size_type>(1) << static_cast<element_size_type>(index);

                for (array_size_type i = 0; i < static_cast<array_size_type>(MaxElements); ++i)
                {
                    if (m_masks[i] & mask)
                    {
                        op(arr[i]);
                    }
                }
            }
        }

        /**
         * @brief Returns how many entities are currently active.
         * 
         */
        [[nodiscard]] constexpr auto active_entity_count() const noexcept
        {
            return m_active_entities.size();
        }

        /**
         * @brief Returns whether or not there are any entities active.
         * 
         */
        [[nodiscard]] constexpr auto empty() const noexcept
        {
            return m_active_entities.empty();
        }

        /**
         * @brief Returns the max number of entities supported by the object.
         * 
         */
        [[nodiscard]] constexpr auto max() const noexcept -> array_size_type
        {
            return MaxElements;
        }

        /**
         * @brief Returns how many more entities can be created.
         * 
         */
        [[nodiscard]] constexpr auto remaining_space() const noexcept -> array_size_type
        {
            return max() - active_entity_count();
        }
    private:
        /**
         * @brief Finds the index of the first available entity.
         * 
         * @return array_size_type The slot.
         */
        [[nodiscard]] constexpr auto find_open_slot() const noexcept -> array_size_type
        {
            for (array_size_type i = 0; i < static_cast<array_size_type>(MaxElements); ++i)
            {
                if (m_masks[i] == 0)
                {
                    return i;
                }
            }
            
            return static_cast<array_size_type>(MaxElements);
        }

        /**
         * @brief Marks bit masks to represent an entities elements.
         * 
         * @tparam Members Which elements to use.
         * @param slot The slot of the entity.
         */
        template <std::size_t... Members>
        constexpr auto create_entity_impl(array_size_type slot) noexcept -> void
        {
            if (slot < MaxElements)
            {
                m_masks[slot] = ((static_cast<element_size_type>(1) << static_cast<element_size_type>(Members)) | ... | 0);
            }
        }
    };
}

#endif
