/******************************************************************************/
/*
* @file   main.cpp
* @author Aditya Harsh
* @brief  Example usage.

          Copyright (c) 2019 Aditya Harsh
          Permission is hereby granted, free of charge, to any person obtaining 
          a copy of this software and associated documentation files 
          (the "Software"), to deal in the Software without restriction, 
          including without limitation the rights to use, copy, modify, merge, 
          publish, distribute, sublicense, and/or sell copies of the Software, 
          and to permit persons to whom the Software is furnished to do so, 
          subject to the following conditions:
          The above copyright notice and this permission notice shall be 
          included in all copies or substantial portions of the Software.
          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
          EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
          MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
          NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
          BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN 
          ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
          CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
          SOFTWARE.
*/
/******************************************************************************/

#include <soa.hpp> // C++ 17 required

#include <string>
#include <iostream>

int main()
{
    // arbitrary POD struct
    struct particle
    {
        int position = 0;
        double direction = 0.0;
        float scale = 0.0f;
        long x = 0;
        std::string name;
    };

    // core soa manager
    auto manager = atl::soa<10, particle>();

    // create a new entity with a position and name
    auto e = manager.create_entity<0, 4>();
    e.emplace(5, "e_emplaced"); // emplace arguments
    e.assign(3, "e_assigned"); // copy arguments

    // convert all of e's members into a std::tuple
    auto x = e.get_all();
    std::cout << std::get<1>(x) << '\n';

    auto e2 = manager.create_entity<0, 1, 2, 3, 4>();
    e2.emplace(1, 3.1, -42.3, 100, "e2");
    e2.get_member<0>() = 5; /// get position, and set it to 5
    e2.for_each_member([] (const auto& i) { // iterate through every member of e2
        std::cout << i << '\n';
    });

    manager.for_each<4>([] (const auto& i) { // iterate through the names of all active entities
        std::cout << i << '\n';
    });

    manager.for_each<4>([] (const auto& i) { // iterate through the names of all active and inactive entities
        std::cout << i << '\n';
    }, true);

    return 0;
}
