# SoA (Struct of Arrays) #

SoA is a library that allows for easily converting data structs into a struct of arrays.

Wikipedia:
"Structure of arrays (or SoA) is a layout separating elements of a record (or 'struct' in the C programming language) into one parallel array per field. The motivation is easier manipulation with packed SIMD instructions in most instruction set architectures, since a single SIMD register can load homogeneous data, possibly transferred by a wide internal datapath (e.g. 128-bit). If only a specific part of the record is needed, only those parts need to be iterated over, allowing more data to fit onto a single cache line. The downside is requiring more cache ways when traversing data, and inefficient indexed addressing. (see also: planar image format)."

### Prerequisites
```
SoA requires a C++17 or higher compliant compiler.
```

### Who do I talk to? ###

* Aditya Harsh
* mrsaturnsan@gmail.com

# Usage #
```
#include <soa.hpp> // C++ 17 required

#include <string>
#include <iostream>

int main()
{
    // arbitrary POD struct
    struct particle
    {
        int position = 0;
        double direction = 0.0;
        float scale = 0.0f;
        long x = 0;
        std::string name;
    };

    // core soa manager
    auto manager = atl::soa<10, particle>();

    // create a new entity with a position and name
    auto e = manager.create_entity<0, 4>();
    e.emplace(5, "e_emplaced"); // emplace arguments
    e.assign(3, "e_assigned"); // copy arguments

    // convert all of e's members into a std::tuple
    auto x = e.get_all();
    std::cout << std::get<1>(x) << '\n';

    auto e2 = manager.create_entity<0, 1, 2, 3, 4>();
    e2.emplace(1, 3.1, -42.3, 100, "e2");
    e2.get_member<0>() = 5; /// get position, and set it to 5
    e2.for_each_member([] (const auto& i) { // iterate through every member of e2
        std::cout << i << '\n';
    });

    manager.for_each<4>([] (const auto& i) { // iterate through the names of all active entities
        std::cout << i << '\n';
    });

    manager.for_each<4>([] (const auto& i) { // iterate through the names of all active and inactive entities
        std::cout << i << '\n';
    }, true);

    return 0;
}
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments

* Antony Polukhin for his awesome Boost reflection library!
